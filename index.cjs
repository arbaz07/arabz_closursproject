const limitFunctionCallCount = require("./limitFunctionCallCount.cjs");
const counterFactory = require("./counterFactory.cjs");
const cacheFunction = require("./cacheFunction.cjs");



function callBack(){
    console.log( "Call Back invoked");
}

let result = limitFunctionCallCount(callBack,434);
// console.log(typeof(result));
console.log(result())

//limitFunctionCallCount("a",10)();





//Counter Factory methode call

let resultCF = counterFactory("as");

console.log(resultCF.increment());

console.log(resultCF.increment());

console.log(resultCF.increment());

console.log(resultCF.decrement());



//Cache Function
//Call Back Function
function callBack(str){
    console.log("callBack Function is invoked first time with arg "+str);
    return "Hello    =>   "+str;
}
const functionToInvokeCB=cacheFunction(callBack);

result = functionToInvokeCB("s");
console.log("if String Passed = "+result);

result = functionToInvokeCB(12);
console.log(result);

result = functionToInvokeCB(12);
console.log(result);

result = functionToInvokeCB(callBack);
console.log(result);


result = functionToInvokeCB(function(temp){return temp+2;});
console.log(result);

result = functionToInvokeCB(function(temp){return temp+2;});
console.log(result);


result = functionToInvokeCB(null);
console.log(result);


result = functionToInvokeCB({1:123});
console.log(result);

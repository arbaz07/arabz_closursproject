

function counterFactory(){
    let counter = 0;
    const increment = function inc(){
        counter++;
        return counter;
    }
    const decrement = function dec(){
        counter--;
        return counter;
    }
    return {increment:increment, decrement:decrement}
}

module.exports = counterFactory;


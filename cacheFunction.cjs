
function cacheFunction(callBackFunction){
    if(typeof(callBackFunction)!='function'){
        return [];
    }

    let cacheObject = {};

    //function responsible for invoking CallBack Function
    const invokeCB = function(args){  
        if(args == null || args == undefined || args == NaN){
            return null;
        }

        //Check if argument is present in the object
        //if function is invoked before it will return previou result.
        let ifPresent = checkIfExists(cacheObject,args);
        if (ifPresent!=null){
            return "call back function is already invoked with this argument  =>"+ifPresent;
        }

        //if function is not invocked before with the given argument
        //it will invoke it and store returned result in an object
        //result will be stored in cache object argument as key and result as value
        let result = callBackFunction(args);
        cacheObject[arguments[0]]=result;

        return result;

    }
    return invokeCB;
}

//methode for checking if function is invoked before with giiven argyment
function checkIfExists(object, key){
    if(object.hasOwnProperty(key)){
        return object[key];
    }else{
        return null;
    }
}




module.exports = cacheFunction;





//Call Back Function to Test
function callBack(str){
    console.log("callBack Function is invoked with arg "+str);
    
    return "Hello "+str;
}


//test code
/*
const fun=cacheFunction(function(argss){
    return argss+12;
});
console.log(fun("arbaz"));
console.log(fun(null));

console.log(fun({}));

console.log(fun("arbaz"));

fun("Sohel");
fun("arbaz");
*/



// cacheFunction("sad");
// cacheFunction({1:"asda",2:[1,2,3]});
// cacheFunction(function(){return "sad";});
// cacheFunction(33);
// cacheFunction([1,2,3,4]);

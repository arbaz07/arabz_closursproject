const testCacheFunction = require("../cacheFunction.cjs");

//Call Back Function
function callBack(str){
    console.log("callBack Function is invoked first time with arg "+str);
    return "Hello    =>   "+str;
}


const functionToInvokeCB=testCacheFunction(callBack);

let result = functionToInvokeCB("s");
console.log("if String Passed = "+result);

result = functionToInvokeCB(12);
console.log(result);

result = functionToInvokeCB(12);
console.log(result);

result = functionToInvokeCB(callBack);
console.log(result);



result = functionToInvokeCB(undefined);
console.log(result);


result = functionToInvokeCB(122);
console.log(result);


result = functionToInvokeCB(function(temp){return temp+2;});
console.log(result);

result = functionToInvokeCB(function(temp){return temp+2;});
console.log(result);


result = functionToInvokeCB(null);
console.log(result);


result = functionToInvokeCB({1:123});
console.log(result);

const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function callBack(){
    console.log("Call Back invoked");
}

let result = limitFunctionCallCount(callBack,12);
// console.log(typeof(result));
console.log(result());

//limitFunctionCallCount("a",10)();


let result1 = limitFunctionCallCount(callBack,"s");
console.log(result1());


let result2 = limitFunctionCallCount("sda",null);
console.log(result2());


let result3= limitFunctionCallCount(function(){console.log(23)},12);
console.log(result3());


let result4 = limitFunctionCallCount({},[]);
console.log(result4());